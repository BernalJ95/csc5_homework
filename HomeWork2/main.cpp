/* 
 * File:   main.cpp
 * Author: Jose
 *
 * Created on September 19, 2014, 8:17 AM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    
    //Problem 4 for HW2
    
    //Mad Lib game
    cout << "Enter a name: " << endl;
    string name1;
    cin >> name1;
    
    cout << "Enter another name: " << endl;
    string name2;
    cin >> name2;
    
    cout << "Enter a type of food: " << endl;
    string food;
    cin >> food;
    
    cout << "Enter a number between 100 and 120: " << endl;
    int num1;
    cin >> num1;
    
    cout << "Enter an adjective: " << endl;
    string adjective;
    cin >> adjective;
    
    cout << "Enter a color: " << endl;
    string color;
    cin >> color;
    
    cout << "Enter an animal: " << endl;
    string animal;
    cin >> animal;
    
    
    cout << "Dear " << name1 << "," << endl;
    cout << "I am sorry that I am unable to turn in my homework at this time."
         << " First I ate a rotten " << food << ", which made me turn "
         << color << " and extremely ill. I came down with a fever of " 
         << num1 << ". Next, my " << adjective << " pet " << animal
         << " must have smelled the remains of the " << food 
         << " on my homework because he ate it."
         << " I am currently rewriting my homework and hope you will"
         << " accept it late." << endl;
    cout << "Sincerely, " << endl;
    cout << name2;
        
    
    return 0;
}

