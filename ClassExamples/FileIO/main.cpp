/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 23, 2014, 10:44 AM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    //Write to a file
    //Three steps
    //Step 1 open the file
    ofstream outfile;
    outfile.open("test.txt");
    
    //Step 2 write to a file
    outfile << "Hello World" << endl;
    outfile << "This is the second line";
    
    //Step 3 close the file
    outfile.close();
    
    //read from a file
    ifstream infile;
    
    //open
    infile.open("test.txt");
    
    string word;
    infile >> word; //use just like cin
    
    cout << "The word is: " << word;
    
    
    //close
    infile.close();
    
            
    
    return 0;
}

