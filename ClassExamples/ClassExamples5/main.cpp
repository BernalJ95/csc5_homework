/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 23, 2014, 10:36 AM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    //User controlled loop
    
    string input;
    do
    {
        //get a number from the user
        cout << "Enter a number: ";
        int num;
        cin >> num;
        
        cout << "Do you want to enter a number: "
             << " again? yes/no: ";
        
        cin >> input;
    } while (input == "yes");

    return 0;
}

