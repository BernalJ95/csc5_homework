/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 23, 2014, 10:53 AM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    //constant variable for change
    const int DOLLAR = 100;
    const int QUARTER = 25;
    const int DIME = 10;
    const int NICKEL = 5;
    const int PENNY = 1;
    //Change problem
    //User input
    cout << "Enter owed amount: ";
    double owedAmount;
    cin >> owedAmount;
    
    cout << "Enter the tender: ";
    double amountPaid;
    cin >> amountPaid;
    
    double change = amountPaid - owedAmount;
    
    //convert my change to an integer to use
    //the modulus operator
    //Add an offset to the change
    
    int intChange = (change + .005) * 100;
    cout << "Your change is: " << intChange << endl;
    
    int numDollars = intChange / DOLLAR;
    
    //get the remaining amount of change
    intChange %= DOLLAR;
    
    int numQuarters = intChange / QUARTER;
    intChange %= QUARTER;
    
    int numDimes = intChange / DIME;
    intChange %= DIME;
    
    int numNickels = intChange / NICKEL;
    intChange %= NICKEL;
    
    int numPennies = intChange / PENNY;
    intChange %= PENNY;
    
    cout << "Number of dollars: " << numDollars << endl;
    cout << "Number of quarters: " << numQuarters << endl;
    cout << "Number of dimes: " << numDimes << endl;
    cout << "Number of nickels: " << numNickels << endl;
    cout << "Number of pennies: " << numPennies << endl;
    
    return 0;
}

