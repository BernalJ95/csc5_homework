/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 23, 2014, 12:21 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    //Problem 1
    cout << "Enter your test letter grade: " << endl;
    string testGrade;
    cin >> testGrade;
    
    
    if (testGrade == "A+" or testGrade == "A" or testGrade == "A-" or
        testGrade == "B+" or testGrade == "B" or testGrade == "B-")
    {
        if (testGrade == "A+")
        {
            cout << "Your score is 4.0!" << endl;
        }
        else if (testGrade == "A")
        {
            cout << "Your score is 4.0!" << endl;
        }
        else if (testGrade == "A-")
        {
            cout << "Your score is 3.7!" << endl;
        }    
        else if (testGrade == "B+")
        {
            cout << "Your score is 3.3!" << endl;
        }
        else if (testGrade == "B")
        {
            cout << "Your score is 3.0!" << endl;
        }
        else if (testGrade == "B-")
        {
            cout << "Your score is 2.7!" << endl;
        }
        else 
        {
            cout << "Incorrect grade format." << endl;
        }
    }
    else 
    {
        cout << "Incorrect grade format." << endl;
    }
    
    
    
    //Problem 2
    cout << "This program will find the square root of a given number." << endl;
    cout << "Enter a number: ";
    int n;
    cin >> n;
    double guess;
    cout << "Enter your guess for the root of your previous guess: ";
    cin >> guess;
    while (n = guess)
    {    
    double R = n / static_cast <double>(guess);
    guess = static_cast <double>((guess + R)) / 2;
    n++;
    }
    cout << "The square root of " << n << " is " << guess << endl;
    
    
    
    //Problem 3
 
    cout << "Enter a year to find out if it is a leap year or not: " << endl;
    int userYear;
    cin >> userYear;
    if (userYear % 100 == 0)
    {
        cout << userYear << " is not a leap year." << endl;
    }
    else if (userYear % 400 == 0 || userYear % 4 == 0)
    {
        cout << userYear << " is a leap year." << endl;
    }
    else
    {
        cout << userYear << " is not a leap year." << endl;
    }
    

  
    
     
    //Problem 4
    cout << "Enter a year to find out if it is a leap year or not: " << endl;
    int yearUser;
    cin >> yearUser;
    if (yearUser > 0)
    {
        if (yearUser % 100 == 0)
        {
            cout << yearUser << " is not a leap year." << endl;
        }
        else if (yearUser % 400 == 0 || yearUser % 4 == 0)
        {
            cout << yearUser << " is a leap year." << endl;
        }
        else
        {
            cout << yearUser << " is not a leap year." << endl;
        }
    }
  
    
    
    
    //Problem 5
    
    
    int x = rand() % 100 + 1;
    int numGuess;
    cout << "The computer has chosen a random number "
         << "between 1 and 100." << endl;
    cout << "Enter your guess, you only have 10 tries: ";
    int y = 0;
    for (int y = 0;y < 10;y++)
    {
        cin >> numGuess;
        if (numGuess > x)
        {
            cout << "Too high." << endl;
        }
        else if (numGuess < x)
        {
            cout << "Too low." << endl;
        }
        else if (numGuess = x)
        {
            cout << "You've guessed correctly." << endl;
            break;
        }
        else
        {
            cout << "Incorrect input." << endl;
        }
    }
   
    
    
    
    
    return 0;
}

