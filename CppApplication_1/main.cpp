/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on August 28, 2014, 11:58 AM
 */

#include <cstdlib>

#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    cout << "Hello, my name is Hal!" << endl;
    
    cout << "What is your name?" << endl;
    
    string name;
    
    cin >> name;
    
    cout << "Hello, " << name << ". Nice to meet you";
    
    return 0;
}

