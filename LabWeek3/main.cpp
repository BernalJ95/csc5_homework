/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 11, 2014, 11:10 AM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    //Problem 1 from Lab Week 3
    
    string var1 = "1";
    int var2 = 2;
    cout << var1 << "+" << var1 << "=" << var1 + var1 << endl;
    cout << var2 << "+" << var2 << "=" << var2 + var2 << endl;
    
    //Problem 2 from Lab Week 3
    
    cout << "Enter the number of singles for player: " << endl;
    
    int singles;
    cin >> singles;
    
    cout << "Enter the number of doubles for player: " << endl;
    
    int doubles;
    cin >> doubles;
    
    cout << "Enter the number of triples for player: " << endl;
    
    int triples;
    cin >> triples;
    
    cout << "Enter the number of home runs for player: " << endl;
    
    int homeRuns;
    cin >> homeRuns;
    
    cout << "Enter the number of at-bats for player: " << endl;
    
    int atBats;
    cin >> atBats;
    
    // Slugging percentage for said player
    cout << "Your players slugging percentage is "; 
    double avg = (singles + 2 * doubles + 3 * triples + 4 * homeRuns) /static_cast <double>(atBats);
    cout << avg;
    cout << "%" << endl;
    
    
    //Problem 3 from Lab Week 3
    
    cout << "Enter 2 numbers that are both less than 1000: " << endl;
    
    int firstNum;
    int secondNum;
    
    
    cin >> firstNum >> secondNum;
    
    cout << "X = " << firstNum << " " << "Y = " << secondNum << endl;
    
    int temp = firstNum;
    firstNum = secondNum;
    secondNum = temp;
            
    cout << "X = " << firstNum << " " << "Y = " << temp << endl;
    
    
    
    
    //Problem 4 from Lab Week 4
    
    cout << "If x is equal to 4, then y must be equal to 4 as well." << endl;
    
    int x = 4;
    int y = 0;
    
    if(x==4)
    {
        y = 4;
    }
    else if(x==9)
    {
        y = 5;
    }
    else
    {
        y = 6;
    }
    
    cout << "X = " << x << " " << "Y = " << y << endl;
    
   
    return 0;
}


