/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 2, 2014, 11:39 AM
 */

#include <cstdlib>

#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    cout << "How many apples do you want?" << endl;
    
    int numApples;
    
    cin >> numApples;
    
    cout << "How many people are there?" << endl;
    
    int numPeople;
    
    cin >> numPeople;
    
    cout << "There should be "
         << numApples / numPeople
         << " apples per person." << endl;
    return 0;
}

