/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 2, 2014, 11:59 AM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    //Lab Question 1
    cout << "Hello, my name is Hal!" << endl;
    
    cout << "What is your name?" << endl;
    
    string name;
    
    cin >> name;
    
    cout << "Hello, " << name << ". I am glad to meet you." << endl;
    
    
    
    //Lab question 2
     int a = 5;
    int b = 10;
    
    cout << "a: " << a <<" "<< "b: " << b << endl;
    
    cout << "a: " << b <<" "<< "b: " << a << endl;
    
    
    
    
    //Lab Question 3
    cout << "How many meters is it from here to the door?" << endl;
    
    int meters;
    
    cin >> meters;
    
    double miles = 1609.344;
    
    cout << meters / miles << " miles." << endl;
    
    double feet = 3.281;
    
    cout << meters * feet << " feet." << endl;
    
    double inches = 39.370;
    
    cout << meters * inches << " inches." << endl;
    
    
    
    //Lab Question 4
    cout << "How many pencils are on your desk, and add that to the 5 pencils you have in your hand?" << endl;
    
    int variable;
    
    cin >> variable;
    
    int numPencils;
    
    numPencils = 5;
    
    cout << "You have " << variable + numPencils << " pencils." << endl;
    
    
    
    //Lab Question 5
    cout << "Enter your three test scores in this format (_ _ _) ";
    
    int firstScore, secondScore, thirdScore;
    
    cin >> firstScore >> secondScore >> thirdScore;
    
    cout << "Average score: " << (firstScore + secondScore + thirdScore) / 3 << endl;
    
    
    
    
    //Lab Question 6
    cout << "Enter any number between the numbers 1 and 10000: ";
    
    int x;
    
    cin >> x;
    
    cout << x;
  
    return 0;
}

