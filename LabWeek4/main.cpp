/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 18, 2014, 11:06 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    //Problem 1
    cout << "Enter your test score number: " << endl;
    int gradeNum;
    cin >> gradeNum;
    if ( 100 >= gradeNum && gradeNum >=90)
    { 
        cout << "A" << endl;
    }
    else if ( 89 >= gradeNum && gradeNum >= 80)
    {
        cout << "B" << endl;
    }
    else if ( 79 >= gradeNum && gradeNum >=70)
    {
        cout << "C" << endl;
    }
    else if ( 69 >= gradeNum && gradeNum >=60)
    {
        cout << "D" << endl;
    }
    else
    {
        cout << "F"<< endl;
    }
    
    
    
    // Problem 2
    //2a
    int x=0;
    while (x < 10)
    {
        cout << x << endl;
        x++;
    }
    
    //2b
    
    //changing gradeNum to scoreNum for error issues
    
    cout << "Enter your test score numbers: " << endl;
    
    
    int scoreNum;
    cin >> scoreNum;
    
    while (scoreNum <= 100)
    {
        cout << "Test score not valid." << endl;
        cin >> scoreNum;
        
    }
   
    if ( 100 >= scoreNum && scoreNum >=90)
    { 
        cout << "A"<< endl;
    }
    else if ( 89 >= scoreNum && scoreNum >= 80)
    {
        cout << "B" << endl;
    }
    else if ( 79 >= scoreNum && scoreNum >=70)
    {
        cout << "C" << endl;
    }
    else if ( 69 >= scoreNum && scoreNum >=60)
    {
        cout << "D" << endl;
    }
    else
    {
        cout << "F" << endl;
    }
    
    
    
    
    //Problem 3
     
    int num;
    for (int x = 0; x < 10; x++)
    {
        cout << "Enter a number: " << endl;
        cin >> num;   
    } 
    
    //Problem 4
    
    cout << "Enter your test letter grade: ";
    string testGrade;
    cin >> testGrade;
    string A;
    string B;
    
    
    if ("A+" == testGrade)   
    {
        cout << "100+";
    }
    else if (A == testGrade)
    {
        cout << "93 - 100";
    }
    else if ("A-" == testGrade)
    {
        cout << "90 - 92.9";
    }
    else if ("B+" == testGrade)
    {
        cout << "87 - 89.9";
    }
    else if (B == testGrade)
    {
        cout << "83 - 86.9";
    }
    else if ("B-" == testGrade)
    {
        cout << "80 - 82.9";
    }
    
    
    return 0;
    
}


